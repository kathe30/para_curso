$(function () {
    /* Para activar el tooltip y popver*/
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();

    /* Para modificar la velocidad del carrusel*/
    $(".carousel").carousel({
      interval:4000
    });
  });

  /* Eventos en el modal*/
  $('#contacto').on('show.bs.modal',function(e){
    console.log("El modal se está mostrando");
    /* Esto deshabilitara el boton y luego se cambiara de color*/
    $('#contactoBtn').prop('disabled',true)
    $('#contactoBtn').removeClass('btn-modal')
    $('#contactoBtn').addClass('btn btn-primary')
  })

  $('#contacto').on('shown.bs.modal',function(e){
    console.log("El modal se mostró")
  })
  $('#contacto').on('hide.bs.modal',function(e){
    console.log("El modal se está ocultando")
  })
  $('#contacto').on('hidden.bs.modal',function(e){
    console.log("El modal se oculto")
    /* Esto habilitara el boton y luego se colocara el color inicial*/
    $('#contactoBtn').prop('disabled',false)
    $('#contactoBtn').removeClass('btn btn-primary')
    $('#contactoBtn').addClass('btn-modal')
})